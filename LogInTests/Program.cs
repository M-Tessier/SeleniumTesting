﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LogInTests
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }

        [SetUp]
        public void Initialize()
        {
            PropertiesCollection.driver = new ChromeDriver();
            PropertiesCollection.driver.Navigate().GoToUrl("http://10.8.232.192:7070");
        }

        [Test]
        public void XtractLogInTest()
        {
            SeleniumSetMethods.EnterText("user", "michael.tessier@exfo.com");
            SeleniumSetMethods.EnterText("pass", "Gameon2018!");
            SeleniumSetMethods.Click("login-btn");
        }

        [TearDown]
        public void CleanUp()
        {
            PropertiesCollection.driver.Close();
        }
    }
}
