﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace LogInTests
{
    class SeleniumSetMethods
    {
        public static void EnterText(string elementId, string value)
        {
                PropertiesCollection.driver.FindElement(By.Id(elementId)).SendKeys(value);
        }

        public static void Click(string elementId)
        {
                PropertiesCollection.driver.FindElement(By.Id(elementId)).Click();
        }
    }
}
